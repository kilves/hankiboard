require 'test_helper'

class ApplicationTest < ActionController::TestCase
    test "should get root" do
        get :root
        assert_response :success
        assert_select "title", "Hanki board software"
    end
end
