require 'test_helper'

class ApplicationTest < ActionController::TestCase
    setup do
        @controller = ApplicationController.new
    end
    test "should get root" do
        get :hello
        assert_response :success
        assert_select "title", "Hanki board software"
    end
end
