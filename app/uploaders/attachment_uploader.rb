require "carrierwave/processing/rmagick"
class AttachmentUploader < CarrierWave::Uploader::Base

  # Include RMagick or MiniMagick support:
  # include CarrierWave::MiniMagick
  include CarrierWave::Video
  #include CarrierWave::Video::Thumbnailer
  include CarrierWave::RMagick
  process :store_dimensions
  # Choose what kind of storage to use for this uploader:
  storage :file
  # storage :fog

  # Override the directory where uploaded files will be stored.
  def store_dir
    "../files/#{model.id}"
  end

  # Provide a default URL as a default if there hasn't been a file uploaded:
  # def default_url
  #   # For Rails 3.1+ asset pipeline compatibility:
  #   # ActionController::Base.helpers.asset_path("fallback/" + [version_name, "default.png"].compact.join('_'))
  #
  #   "/images/fallback/" + [version_name, "default.png"].compact.join('_')
  # end

  # Process files as they are uploaded:
  # process :scale => [200, 300]
  #
  # def scale(width, height)
  #   # do something
  # end

  # Create different versions of your uploaded files:

  version :thumb, :if => :image? do 
    process :remove_animation
    process :resize_to_limit => [200, 200]
  end
  
  process :set_content_type_webm
  
  def set_content_type_webm(*args)
    self.file.instance_variable_set(:@content_type, "video/webm")
  end
  
  
  # Add a white list of extensions which are allowed to be uploaded.
  # For images you might use something like this:
  def extension_white_list
    %w(jpg jpeg gif png svg mp3 mp4 webm ogv ogg 3gp m4v m4a wav m3u )
  end

  # Override the filename of the uploaded files:
  # Avoid using model.id or version_name here, see uploader/store.rb for details.
  # def filename
  #   "something.jpg" if original_filename
  # end

  private
  
  def store_dimensions
  end
  
  protected
  
  def image?(new_file)
    new_file.content_type.start_with?("image")
  end

  def gif?(new_file)
    new_file.content_type == "image/gif"
  end
  
  def audio?(new_file)
    new_file.content_type.start_with? 'audio'
  end
  
  def video?(new_file)
    new_file.content_type.start_with? 'video'
  end

  def remove_animation
    manipulate! do |img, index|
      if img.mime_type == "image/gif"
        img.collapse!
      end
      img
    end
  end
end
