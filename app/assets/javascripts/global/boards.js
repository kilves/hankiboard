$(document).ready(function() {
  poststore = {};
  textstore = {};
});

var settings_up = false;
var login_up = false;
var create_up = false;
var profile_up = false;

$(document).on("click", "a.thumb", function( event ){
  event.preventDefault();
  var t = event.target;
  var regexp=/"\/files\/(\w+)\//
  var mat=regexp.exec(t.outerHTML);
  var foo=1;
  if(mat[1]=="thumb") {
    var rep="\"/files/orig/"
    $(t).replaceWith(t.outerHTML.replace(regexp, rep));
  } else if(mat[1]=="orig") {
    var rep="\"/files/thumb/";
    $(t).replaceWith(t.outerHTML.replace(regexp, rep));
  }
});

$(document).on("click", "a.thumbgif", function( event ){
  event.preventDefault();
  var t = event.target;
  var regexp=/"\/attachments\/posts\/attachments\/(\d+)\/(\w+)/;
  var mat=regexp.exec(t.outerHTML);
  if(mat[2]=="thumb") {
    t.outerHTML=t.outerHTML.replace("thumb/short", "original");
  } else if(mat[2]=="original") {
    t.outerHTML=t.outerHTML.replace("original", "thumb/short");
  }
});

// Handle hiding sidebar
$(document).on("click", "#hide-sidebar", function( event ) {
  event.preventDefault();
  var t = $(this);
  $("#left").toggle("slide");
  if($("#right").css("margin-left")=="8px") {
    $("#right").animate({"margin-left": "208px"}, 400);
    $("#hide-sidebar").animate({"left": "200px"}, 400);
    $(".boardnav").animate({"left": "200px"}, 400);
    $.get("/user/edit", { show_sidebar: "1" });
  } else if($("#right").css("margin-left")=="208px") {
    $("#right").animate({"margin-left": "8px"}, 400);
    $("#hide-sidebar").animate({"left": "0px"}, 400);
    $(".boardnav").animate({"left": "0px"}, 400);
    $.get("/user/edit", { show_sidebar: "0" });
  }
});
$(document).on("click", "#settings-icon", function(event) {
  event.stopPropagation();
  if(settings_up) {settings_up=false;} else {settings_up = true;}
  console.log("settings: "+settings_up);
  console.log("called");
  if(login_up) {
    login_up = false;
    $("#login-popup").toggle("slide", {direction: "up"});
  }
  if(create_up) {
    create_up = false;
    $("#create-popup").toggle("slide", {direction: "up"});
  }
  if(profile_up) {
    profile_up = false;
    $("#profile-popup").toggle("slide", {direction: "up"});
  }
  event.preventDefault();
  $("#settings-popup").toggle("slide", {direction: "up"});
});
$(document).on("click", "#dynamic-login", function(event) {
  if(login_up) {login_up=false;} else {login_up = true;}
  if(settings_up) {
    settings_up = false;
    $("#settings-popup").toggle("slide", {direction: "up"});
  }
  if(create_up) {
    create_up = false;
    $("#create-popup").toggle("slide", {direction: "up"});
  }
  event.preventDefault();
  $("#login-popup").toggle("slide", {direction: "up"});
});
$(document).on("click", "#dynamic-profile", function(event) {
  if(profile_up) {profile_up=false;} else {profile_up = true;}
  if(settings_up) {
    settings_up = false;
    $("#settings-popup").toggle("slide", {direction: "up"});
  }
  event.preventDefault();
  $("#profile-popup").toggle("slide", {direction: "up"});
});
$(document).on("click", "#dynamic-create", function(event) {
  if(create_up) {create_up=false;} else {create_up = true;}
console.log("create: "+settings_up);
  if(settings_up) {
    settings_up = false;
    $("#settings-popup").toggle("slide", {direction: "up"});
  }
  if(login_up) {
    login_up = false;
    $("#login-popup").toggle("slide", {direction: "up"});
  }
  event.preventDefault();
  $("#create-popup").toggle("slide", {direction: "up"});
});
$(document).on("click", "#dynamic-boards", function(event) {
  event.preventDefault();
  $("#boards-popup").toggle("slide", {direction: "up"});
});

// Handle thread expand and contract
$(document).on("click", ".expand", function( event ){
  event.preventDefault();
  var temp_this = $(this);
  var text = $(this).children("a");
  var thread = $(this).parents(".thread");
  var id = parseInt(thread.children(".first").attr("id").slice(7));
  if(!(typeof poststore[id] === "undefined")) {
    var oldposts_temp = thread.find(".answercontainer").detach();
    thread.children(".answers").append(poststore[id]);
    poststore[id] = oldposts_temp;
    text.html("Hide expanded messages");
    temp_this.attr("class", "contract");
  } else {
    $.get("/expand", { id: id }, function(data) {
      poststore[id] = thread.find(".answercontainer").detach();
      thread.children(".answers").append(data);
      text.html("Hide expanded messages");
      temp_this.attr("class", "contract");
    });
  }
});

//delete confirm
$(document).on("click", "#deleteboard", function(event){
  var confirm = confirm("Are you sure you want to delete this board? All the posts will be deleted as well!");
  if(confirm){
    $.get($(this).attr("href"));
  }
});

$(document).on("click", ".contract", function( event ) {
  event.preventDefault();
  var text = $(this).children("a");
  var thread = $(this).parents(".thread");
  console.log(text);
  var id = thread.children(".first").attr("id").slice(7);
  oldposts_temp = thread.find(".answercontainer").detach();
  thread.children(".answers").append(poststore[id]);
  poststore[id] = oldposts_temp;
  text.html(poststore[id].length-3+" messages omitted.");
  $(this).attr("class", "expand");
});

$(document).ready(function(){
  // check the hash
  h = location.hash;
  console.log(h);
  if(h.substr(0,3)=="#q_") {
    msgid=h.substr(3);
    $("#post_message").val($("#post_message").val()+">>"+msgid+"\r\n");
    $("html,body").scrollTop(0);
  } else if(h.substr(0,1)=="#") {
    var msgid = h.substr(1);
    var element = $("#answer-"+msgid);
    $(".highlight").attr("class", "answer");
    if(element.attr("class") != "first") {
      element.attr("class", "highlight");
      window.scrollTo(element.offset().top, 0);
    }
  }
  // update tooltip positions
  $(document).mousemove(function(event){
    $(".singlepost-tooltip").css("top", event.pageY);
    $(".singlepost-tooltip").css("left", event.pageX);
  });
  // Show singlepost tooltip while hovering over quotes
  
  $(".reference").mouseenter(function() {
    var id = $(this).find("a").html().slice(8);
    $(".singlepost-tooltip").css("display", "inline-block");
    $.get("/singlepost/"+id, function(data) {
      $(".singlepost-tooltip").html(data);
    });
  });
  
  $(".reference").mouseout(function() {
    $(".singlepost-tooltip").css("display", "none");
    $(".singlepost-tooltip").html("");
  });
  
  // Limit post length
  $(".post").each(function(){
    var len=$(this).find("p").html().length;
    if(len>=3000) {
      var newpost = $(this).find("p").html().slice(0, 2899);
      var restofpost = $(this).find("p").html().slice(2900);
      newpost+='<div class="expandPost"><a href="">click here to show full post ('+String(restofpost.length)+' remain)</a></div>';
      newpost+='<span class="hidden">'+restofpost+'</span>';
      $(this).find("p").html(newpost);
    }
  });
});

$(document).on("click", ".reference", function(event) {
  event.preventDefault();
  var id = $(this).find("a").html().slice(8);
  var src = window.location.href;
  $.get("/jsrdr", {id: id, src: src}, function(data) {
    var cmd = data.slice(0, 1);
    data.trim();
    if(cmd=="N") {
      // Do nothing
    } else if(cmd=="R") {
      //Redir to ref
      window.location.href = "http://pohjoislauta.fi/"+data.slice(2);
    } else if(cmd=="S") {
      // Stay on page and highlight post
      window.location.href = window.location.href + "#" + String(id);
    } else {
      // Do nothing
    }
  });
});

// Report post
$(document).on("click", ".report_post", function(event){
  event.preventDefault();
  var id = $(this).attr("href").slice(8);
  $(".report").css("display", "block");
  $(".temporary").remove();
  $("#report_post").append('<input class="temporary" type="hidden" name="id" value="'+id+'">');
});

/*
$(".reference").mouseenter(function() {
    t = $(this).parent("p").children(".singlepost-tooltip");
    alert("ok");
    if(t.length) {
      t.css("display", "block");
    } else {
      id = $(this).parent("div[id^=answer]").attr("id").slice(6);
      $(this).parent("p").append('<div class="singlepost-tooltip"></div>');
      $(this).parent("p").css("display", "block");
      alert("ok");
      $.get("/singlepost/"+id, function(data) {
        $(this)parent("p").children(".singlepost-tooltip").append(data);
      });
    }
  });
  
  $(".reference").mouseout(function() {
    $(this).parent("p").children(".reference-tooltip").css("display"; "none");
  });
 */

// Click to show all text
$(document).on("click", ".expandPost", function(event){
  event.preventDefault();
  var t = $(event.target).parent().parent();
  t.find(".hidden").contents().unwrap();
  t.find(".expandPost").remove();
});

// Handle thread hiding
$(document).on("click", "a[id^=hide_thread]", function(event) {
  event.preventDefault();
  var t = $(event.target);
  var id = /(\d+)/.exec(t.parents(".hide_thread").attr("id"))[1];
  $.get("/hide_thread", { id: id }, function(data) {
    if(t.parents(".answercontainer").length==0) {
      t.parents(".thread").replaceWith(data);
    } else if(t.parents(".answercontainer").length==1) {
      t.parents(".answercontainer").replaceWith(data);
    }
  });
});

// Remove board from subscribed
$(document).on("click", ".unsub", function(event) {
  event.preventDefault();
  var t = $(event.target).parents(".ukkoElement");
  var id = t.attr("id");
  $.get("/unsubscribe", { id: id }, function(data) {
    t.toggle("fade")
    t.remove();
  });
});

// Add board to subscribed
$(document).on("click", ".sub", function(event) {
  event.preventDefault();
  var t = $(event.target).parents(".categoryElement");
  var addTo = $(".ukko");
  var id = t.attr("id");
  $.get("/subscribe", { id: id }, function(data) {
    $("#ukko").append(data);
    t.children(".sub").empty();
  });
});

// Selecting posts
$(document).on("click", "input[id^=select]", function(event) {
  var checked = $("input[id^=select]:checked");
  if(checked.length>0) {
    $("#post-operations").css("display", "block");
  } else {
    $("#post-operations").css("display", "none");
  }
});

$(document).on("click", "#select-all", function(event) {
  event.preventDefault();
  $("input[id^=select]").prop("checked", true);
  $(this).attr("id", "deselect-all");
  $(this).html("Deselect all");
});

$(document).on("click", "#deselect-all", function(event) {
  event.preventDefault();
  $("input[id^=select]").prop("checked", false);
  $(this).attr("id", "select-all");
  $(this).html("Select all");
});

$(document).on("click", "#post-delete", function(event) {
  event.preventDefault();
  var postids = new Array;
  postids[0] = "delete";
  $("input[id^=select]:checked").each(function (i, e) {
    postids[i+1] = e.id.slice(7);
  });
  var html = '<input class="temporary" type="hidden" name="posts" value="'+postids.join('-')+'">';
  $("#deleteposts").append(html);
  $("form#deleteposts").submit();
  $(".temporary").remove();
});

// SETTINGS N SHIT
$(document).on("change", ".settings-select", function() {
  var val = $(this).attr("id");
  if(val=="stylesheet") {
    $.get("/user/edit", { stylesheet: $(this).val() }, function(data) {
      $('link[id="dynamic-stylesheet"]').attr("href", data);
    });
  } else if(val=="post_noko") {
    $.get("/user/edit", { post_noko: $(this).val() });
  } else if(val=="locale") {
    $.get("/user/edit", { locale: $(this).val() });
  }
});
shitfunc = function(el) {
  return String($(el).is(":checked"));
}
$(document).on("change", "#user_save_scroll", function(){$.get("/user/edit", {save_scroll: String($(this).is(":checked"))})});
$(document).on("change", "#user_hide_names", function(){$.get("/user/edit", {hide_names: String($(this).is(":checked"))})});
$(document).on("change", "#user_hide_region", function(){$.get("/user/edit", {hide_region: String($(this).is(":checked"))})});
$(document).on("change", "#user_autoload_media", function(){$.get("/user/edit", {autoload_media: String($(this).is(":checked"))})});
$(document).on("change", "#user_autoplay_gifs", function(){$.get("/user/edit", {autoplay_gifs: String($(this).is(":checked"))})});

// download posts
$(document).on("click", "#image-download", function(event) {
  event.preventDefault();
  var postids = new Array;
  postids[0] = "download";
  $("input[id^=select]:checked").each(function (i, e) {
    postids[i+1] = e.id.slice(7);
  });
  name = $("#filename-field").val();
  pids = postids.join('-');
  var html1 = '<input class="temporary" type="hidden" name="posts" value="'+pids+'">';
  var html2 = '<input class="temporary" type="hidden" name="filename" value="'+name+'">';
  $("#deleteposts").append(html1);
  $("#deleteposts").append(html2);
  $("form#deleteposts").submit();
  $(".temporary").remove();
});

//on hash change
$(window).hashchange(function() {
  h = location.hash;
  console.log(h);
  if(h.substr(0,3)=="#q_") {
    msgid=h.substr(3);
    $("#post_message").val($("#post_message").val()+">>"+msgid+"\r\n");
    $("html,body").scrollTop(0);
  } else {
    var msgid = h.substr(1);
    var element = $("#answer-"+msgid);
    $(".highlight").attr("class", "answer");
    if(element.attr("class") != "first") {
      element.attr("class", "highlight");
      $.scrollTo(element);
    }
  }
});
