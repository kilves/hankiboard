$(document).ready(function() {
  var h = window.location.hash;
  changeto(h.substr(1));
});
changeto = function(str) {
  if(!["", "general", "account", "boards"].indexOf(str)) {
    console.log("not in array ("+str+" not in "+["", "general", "account", "boards"]);
    return;
  }
  console.log(str);
  $(".tab-active").attr("class", "tab-hidden");
  $(".tablink-active").attr("class", "tablink");

  $("#"+str+"-tablink").attr("class", "tablink-active");
  $("#"+str).attr("class", "tab-active");
}
$(document).on("click", ".tablink", function(event) {
  changeto($(this).attr("href").substr(1));
});

$(document).on("click", "a#unhide", function(event) {
  event.preventDefault();
  var t = $(event.target);
  var id = t.parents(".hidden_thread").attr("id");
  $.get("/unhide", {id: id}, function( data ) {
    t.parent().parents(".hidden_thread").replaceWith(data);
  });
});
