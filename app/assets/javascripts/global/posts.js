$(document).ready(function(){
  // Handle thumbnail enlargement
  $("a#thumb").click(function( event ){
    event.preventDefault();
    var t = event.target;
    var regexp=/"\/attachments\/posts\/attachments\/(\d+)\/(\w+)/
    var mat=regexp.exec(t.outerHTML);
    if(mat[2]=="thumb") {
      var rep="\"/attachments/posts/attachments/"+mat[1]+"/original";
      $(t).replaceWith(t.outerHTML.replace(regexp, rep));
    } else if(mat[2]=="original") {
      var rep="\"/attachments/posts/attachments/"+mat[1]+"/thumb";
      $(t).replaceWith(t.outerHTML.replace(regexp, rep));
    }
  });
  
  $(".contract").click(function( event ){
    event.preventDefault();
    var t = event.target;
    var par = $(t).parent().parent();
    var chil = par.children(".answer");
    var posts = chil.slice(0, chil.length-3);
    posts.remove();
  });
  
});
