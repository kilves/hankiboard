class String
  def shorten_path(len)
    arr = self.split(".")
    arr = [arr[0, arr.length-1].join("."), arr.last]
    arr[0] = arr[0][0, len]+"[..]"
    return arr.join(".")
  end
end
