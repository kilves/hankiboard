# The main controller. This is always executed.
# Contains mainly user profile check stuff.
class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  require 'openssl'
  require 'hash'
  protect_from_forgery with: :exception
  
  # Check user profile on every page load
  before_action :profilecheck, :boardlist
  
  def hello
      render(text: "Hello", :layout => 'application')
  end
  
  def frontpage
    @posts = FrontpagePost.all.order(created_at: :desc)
    render file: "frontpage"
  end

  def redirect
    post = Post.find(params[:id])
    threadid = post.thread_id
    if(threadid==0)
      redirect_to "/"+post.board.url+"/"+post.id.to_s+"#n_"+post.id.to_s and return
    else
      redirect_to "/"+post.board.url+"/"+threadid.to_s+"#n_"+post.id.to_s and return
    end
  end
  
  private
  
  def profilecheck
    unless request.original_url.include?("/files/") || params[:url]=="jsrdr"
      unless cookies.signed[:hb_series] == nil
        puts "Token given"
        l = Login.find_by_series(cookies.signed[:hb_series])
        if !l.nil?
          @user = l.user
        end
      end
      if @user.nil?
        @user = createuser
      else
        @user.ip = Digest::SHA1.hexdigest(request.remote_ip+Rails.application.config.ip_salt)
        @user.last_load = DateTime.current
        @user.last_page = request.original_url
        if @user.valid?
          @user.save!
          @user.online = @user.online_now
        end
        @user.save!
      end
      @overboard_boards = @user.overboard.boards
    end
  end
  
  def boardlist
    @brds = Board.where.not(hidden: true)
  end
  
  private
  
  def createuser
    user = User.new
    user.uid = SecureRandom.uuid
    ip = request.remote_ip
    if ip.nil?
      raise "ERROR: Invalid IP."
    end
    user.ip = Digest::SHA1.hexdigest(request.remote_ip+Rails.application.config.ip_salt)
    same_users = User.where(password: "").where(ip: user.ip).order(last_load: :asc)
    if same_users.count > 9
      same_users.first.logins.destroy_all
      same_users.first.overboard.destroy
      same_users.first.replied.destroy
      same_users.first.destroy # do not allow more than 10 users from same IP at a time
    end
    user.online = true
    user.last_load = DateTime.current
    user.last_page = request.original_url
    user.site_style = "default"
    user.show_sidebar = 1
    user.show_postform = true
    user.save_scroll = false
    user.autoload_media = true
    user.autoplay_gifs = false
    user.sfw = false
    user.locale = "fi_FI.UTF-8"
    user.timezone = "Europe/Helsinki"
    user.hide_names = false
    user.autoload_media = false
    user.autoplay_gifs = false
    user.hide_region = false
    user.hide_ads = 0
    user.hide_browserwarning = false
    user.uname = SecureRandom.hex(32).to_s
    user.password = ""
    user.password_salt = ""
    if user.valid?
      user.save!
      # create boards
      overboard = Overboard.new
      overboard.user = user
      overboard.boards = Board.where(category: 1)
      overboard.save!
      replied = Replied.new
      replied.user = user
      replied.save!

      # create login
      l = Login.new
      user_agent = UserAgent.parse(request.user_agent)
      l.user = user
      l.browser = user_agent.browser+" "+user_agent.version+" ("+user_agent.platform+")"
      l.token = SecureRandom.hex
      l.series = SecureRandom.hex
      l.save!
      cookies.permanent.signed[:hb_token] = l.token
      cookies.permanent.signed[:hb_series] = l.series
    else
      raise "Critical error in user creation: user is not in valid format" and return
    end
    
    return user
  end
end
