class RepliedController < ApplicationController
  def show
    user = @user
    @board = Array.new
    comm = params[:url].split("-")
    if comm.length == 1
      page = 0
    else
      page = comm[1].to_i
    end
    @pages = (user.replied.post_ids.count().to_f/15.0).ceil
    @page = page
    pids = user.replied.post_ids.order(id: :desc).offset(page*15).limit(15)
    pids.each do |p|
      po = nil
      post = Post.find(p.pid)
      unless Hide.where(uid: user.uid).any? {|h| h.post==post.id}
        replies = Post.where(thread_id: p.pid).order(id: :asc)
        omitted = replies.length-3
        po = replies.last(3)
        po.unshift(omitted)
        po.unshift(post)
        @board.push(po)
      end
    end
  end
end
