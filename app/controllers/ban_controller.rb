class BanController < ApplicationController
  def create_form
    @ip = params[:ip]
    @id = params[:id]
    @board = Board.find_by(url: params[:url])
  end
  def create
    user = @user
    if user.perm("ban", params[:url])
      if params[:plain_ip].nil? && params[:ip].nil?
        @error = "You must provide either a plain ip or a regular one."
        render file: "error"
      end
      @board = Board.find_by(url: params[:url])
      ban = Ban.new
      post = Post.find(params[:id])
      post.modinfo = params[:notice]
      post.save!
      if params[:plain_ip].nil?
        ban.ip = params[:ip]
      else
        ban.plain_ip = params[:plain_ip]
      end
      ban.ip = params[:ip]
      ban.post_id = params[:id]
      ban.board = @board.id
      ban.reason = params[:reason]
      ban.expires = params[:duration].to_i.seconds.from_now
      ban.banned_by = user.uname
      if ban.valid?
        ban.save!
      else
        @error = ban.errors.messages
        render file: "error"
      end
    else
      @error = "You are not allowed to ban people from this board."
      render file: "error"
    end
  end
  
  def delete
    user = @user
    ban = Ban.find(params[:id])
    board = Board.find(ban.board)
    if user.perm("unban", board.url)
      ban.destroy
      redirect_to "/#{board.url}/mod"
    else
      @error = "You are not allowed to unban people."
      render file: "error"
    end
  end
end
