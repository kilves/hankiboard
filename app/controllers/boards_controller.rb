# A controller to handle boards
class BoardsController < ApplicationController
  layout "application"
  def new
    @board = Board.new
  end
  
  def get_name(url)
    @name = Board.find_by(url).name
  end
  
  # Show a board. This is called every time user types in site.com/something-number or just
  # site.com/something and no other rewrites for it exist. It also handles my threads, replied
  # threads and overboard.
  def show
    # Separate url to page number and board url and setup variables accordingly
    request = params[:url].split("-")
    @threads = Array.new
    if request.length == 1
      @page = 0
    else
      @page = request[1].to_i-1
    end
    ########################################################
    ################## Handle my threads ###################
    ########################################################
    if request[0] == "mythreads"
      ids = Array.new
      @board = Board.new
      @board.url = "mythreads"
      posts = Post
          .where(thread_id: 0)                                    # All thread starting posts
          .where(uid: @user.uid)                                  # That are owned by user
          .where.not(id: Hide.where(uid: @user.uid).select(:post))# and are not hidden
          .where(deleted_time: [0, nil])                               # and not deleted
          .order(bump_time: :desc)                                # descending order
          .offset(15*@page)                                       # with offset on page start
          .limit(15)                                              # only diplay one page
          
      # get pagecount
      @pages = (Post
          .where(thread_id: 0)
          .where(uid: @user.uid)
          .where.not(id: Hide.where(uid: @user.uid).select(:post))
          .where(deleted_time: [0, nil])
          .count.to_f/15.0).ceil
      posts.each do |p|
        po = nil
        ids.push(p.id)
        post = Post
            .where(thread_id: p.id)                                 # All posts in a thread
            .where.not(id: Hide.where(uid: @user.uid).select(:post))# That are not hidden
            .where(deleted_time: [0, nil])                               # or deleted
            .order(id: :asc)                                        # ascending order
        post.each do |x|
          ids.push(x.id)
        end
        omitted = post.length-3 # how many omitted posts
        # populate table with last posts and push to table
        po = post.last(3)
        po.unshift(omitted)
        po.unshift(p)
        @threads.push(po)
      end
      @preloaded_attachments = Attachment.where(post_id: ids)
      render "mythreads/show"
    ########################################################
    ################ Handle replied threads ################
    ########################################################
    elsif request[0] == "repliedthreads"
      ids = Array.new
      @board = Board.new
      @board.url = "repliedthreads"
      postids = @user.replied.post_ids.select(:pid)
      posts = Post
          .where(id: postids)
          .where(deleted_time: [0, nil])
          .where.not(id: Hide.where(uid: @user.uid).select(:post))
          .order(bump_time: :desc)
          .offset(15*@page)
          .limit(15)
      @pages = (Post
          .where(id: postids)
          .where(deleted_time: [0, nil])
          .where.not(id: Hide.where(uid: @user.uid).select(:post))
          .count.to_f/15.0).ceil
      posts.each do |p|
        po = nil
        ids.push(p.id)
        post = Post
            .where(thread_id: p.id)
            .where(deleted_time: [0, nil])
            .where.not(id: Hide.where(uid: @user.uid).select(:post))
            .order(id: :asc)
        post.each do |x|
          ids.push(x.id)
        end
        omitted = post.length-3
        po = post.last(3)
        po.unshift(omitted)
        po.unshift(p)
        @threads.push(po)
      end
      @preloaded_attachments = Attachment.where(post_id: ids)
      render "replied/show"
    ########################################################
    #################### Handle /ukko/ #####################
    ########################################################
    elsif request[0] == "ukko"
      @board = Board.new
      @board.url = "ukko"
      ids = Array.new
      posts = Post
          .where(thread_id: 0)
          .where(deleted_time: [0, nil])
          .where(board_id: @user.overboard.boards.select(:id))
          .where.not(id: Hide.where(uid: @user.uid).select(:post))
          .order(bump_time: :desc)
          .offset(15*@page)
          .limit(15)
      @pages = (Post
          .where(thread_id: 0)
          .where(deleted_time: [0, nil])
          .where(board_id: @user.overboard.boards.select(:id))
          .where.not(id: Hide.where(uid: @user.uid).select(:post))
          .count.to_f/15.0).ceil
      posts.each do |p|
        po = nil
        ids.push(p.id)
        post = Post
            .where(thread_id: p.id)
            .where(deleted_time: [0, nil])
            .where.not(id: Hide.where(uid: @user.uid).select(:post))
            .order(id: :asc)
        post.each do |x|
          ids.push(x.id)
        end
        omitted = post.length-3
        po = post.last(3)
        po.unshift(omitted)
        po.unshift(p)
        @threads.push(po)
      end
      @preloaded_attachments = Attachment.where(post_id: ids)
      render "overboard/show"
    ########################################################
    #################### Handle /all/ ######################
    ########################################################
    elsif request[0] == "all"
      board_ids = Array.new
      Board.where(hidden: true).each do |b|
        board_ids.push(b.id)
      end 
      posts = Post
          .where.not(board_id: board_ids)
          .where(thread_id: 0)
          .where(deleted_time: [0, nil])
          .where.not(id: Hide.where(uid: @user.uid).select(:post))
          .order(bump_time: :desc)
          .offset(15*@page)
          .limit(15)
      @pages = (Post
          .where.not(board_id: board_ids)
          .where(thread_id: 0)
          .where(deleted_time: [0, nil])
          .where.not(id: Hide.where(uid: @user.uid).select(:post))
          .count.to_f/15.0).ceil
      ids = Array.new
      posts.each do |p|
        po = nil
        ids.push(p.id)
        post = Post
            .where(thread_id: p.id)
            .where(deleted_time: [0, nil])
            .where.not(id: Hide.where(uid: @user.uid).select(:post))
            .order(id: :asc)
        post.each do |x|
          ids.push(x.id)
        end
        omitted = post.length-3
        po = post.last(3)
        po.unshift(omitted)
        po.unshift(p)
        @threads.push(po)
      end
      @preloaded_attachments = Attachment.where(post_id: ids)
      @board = Board.new
      @board.url = "all"
      render "overboard/show_all"
    ########################################################
    ################ Handle regular boards #################
    ########################################################
    else
      @board = Board.find_by(url: request[0])
      if @board.nil? || request.length > 2
        redirect_to "/404"
      else
        ids = Array.new
        name = @board.name
        posts = @board
            .posts.where(thread_id: 0)
            .where(deleted_time: [0, nil])
            .where.not(id: Hide.where(uid: @user.uid).select(:post))
            .order(sticky: :desc)
            .order(bump_time: :desc)
            .offset(15*@page)
            .limit(15)
        @pages = (@board
            .posts.where(thread_id: 0)
            .where(deleted_time: [0, nil])
            .where.not(id: Hide.where(uid: @user.uid).select(:post))
            .count.to_f/15.0).ceil
        posts.each do |p|
          po = nil
          if p.thread_id == 0
          ids.push(p.id)
            post = Post
                .where(thread_id: p.id)
                .where(deleted_time: [0, nil])
                .where.not(id: Hide.where(uid: @user.uid).select(:post))
                .order(id: :asc)
            post.each do |x|
              ids.push(x.id)
            end
            omitted = post.length-3
            po = post.last(3)
            po.unshift(omitted)
            po.unshift(p)
            @threads.push(po)
          end
        end
        @threads.push(name)
        @preloaded_attachments = Attachment.where(post_id: ids)
      end
    end
  end
  
  def get_post(id)
    @post = Post.find(id)
  end
  
  def expand
    post=Post.find(params[:id].to_i)
    @posts = Post.where(thread_id: params[:id]).where(deleted_time: nil).where.not(id: Hide.where(uid: @user.uid).select(:post))
    respond_to do |format|
      format.html { render file: "expand", layout: false }
    end
  end
  
  def create
    if params[:url].nil? || params[:name].nil? || params[:description].nil? || params[:namefield].nil?
      return
    end
    @board = Board.new
    @board.url = params[:url]
    @board.name = params[:name]
    @board.description = params[:description]
    @board.postcount = 0
    if params[:international]=="1"
      @board.international = true
    else
      @board.international = false
    end
    @board.locked = false
    if params[:worksafe]=="1"
      @board.worksafe = true
    else
      @board.worksafe = false
    end
    @board.namefield = params[:namefield].to_i
    if params[:hide_sidebar]=="1"
      @board.hide_sidebar = true
    else
      @board.hide_sidebar = false
    end
    if params[:hidden]=="1"
      @board.hidden = true
    else
      @board.hidden = false
    end
    user = @user
    if !user.password.empty?
      @board.user = user
    else
      return nil
    end
    @board.category_id = 2
    if @board.valid?
      @board.save!
    end
  end
  
  def update
    user = @user
    @board = Board.find_by(url: params[:url])
    @saved = false
    if request.method == "POST" && @board.user == user
      @board.name = params[:name]
      @board.description = params[:description]
      @board.namefield = params[:namefield].to_i
      @board.international = {"0"=>false,"1"=>true}[params[:international]]
      @board.worksafe = {"0"=>false,"1"=>true}[params[:worksafe]]
      @board.hide_sidebar = {"0"=>false,"1"=>true}[params[:hide_sidebar]]
      if params[:hidden]=="1"
        @board.hidden = true
      else
        @board.hidden = false
      end
      if @board.valid?
        @board.save!
        @saved = true
      end
    elsif @board.user != user
      @error = "You don't have permission to change board settings"
      render file: "error"
    end
  end
  
  def list
    @boards = Board.where(hidden: false).order(postcount: :desc)
    @categories = Category.all.order(order: :desc)
  end
  
  def threadlist
    @user = User.find_by(session[:url])
    @board = Board.find_by(url: params[:url])
    @threads = @board.posts
        .where(thread_id: 0)
        .where(deleted_time: nil)
        .where.not(id: Hide.where(uid: @user.uid).select(:post))
        .order(bump_time: :desc)
        .limit(150)
    render "boards/threadlist"
  end
  
  def confirm_delete
    @board = Board.find_by(url: params[:url])
  end
  
  def delete
    board = Board.find_by(url: params[:url])
    user = @user
    if(board.user==user)
      board.posts.each do |p|
        p.deleted_time = DateTime.now
        p.save!
      end
      board.destroy
    end
    redirect_to "/" and return
  end
end
