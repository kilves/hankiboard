class ReportReasonController < ApplicationController
  def create
    user = @user
    board = Board.find(params[:board])
    if user.perm("change_rules", board.url)
      reason = ReportReason.new
      reason.name = params[:name]
      reason.description = params[:description]
      reason.board = board
      if reason.valid?
        reason.save!
        redirect_to "/#{board.url}/mod"
      else
        @error = reason.errors.messages
      end
    else
      @error = "You are not allowed to change rules in this board."
    end
  end
end
