class OverboardController < ApplicationController
  def show
    user = @user
    @board = Array.new
    page = 0
    page = params[:page].to_i-1 if !params[:page].nil? || params[:page].to_i != 0
    posts = Post
        .where(board_id: user.overboard.boards.select(:id))
        .where.not(id: Hide.where(uid: user.uid).select(:post))
        .order(bump_time: :desc)
        .offset(15*page)
        .limit(15)
    @pages = posts.count
    posts.each do |p|
      po = nil
      if p.thread_id == 0
        post = Post
          .where(thread_id: p.id)
          .where.not(id: Hide.where(uid: user.uid).select(:post))
          .order(id: :asc)
        omitted = post.length-3
        po = post.last(3)
        po.unshift(omitted)
        po.unshift(p)
        @board.push(po)
      end
    end
    @page = page
  end
  
  def delete
    overboard = @user.overboard
    overboard.boards = overboard.boards.where.not(id: params[:id])
    redirect_to request.referrer
  end
  
  def subscribe
    overboard = @user.overboard
    @board = Board.find(params[:id])
    overboard.boards << @board
    overboard.save!
    redirect_to request.referrer
  end
end
