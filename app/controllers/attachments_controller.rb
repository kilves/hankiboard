# A controller to handle attachments
class AttachmentsController < ApplicationController
  
  def create
    @attachment = Attachment.create( attachment_params )
  end

  def show
    att = Attachment.find_by(name: params[:name])
    
    if params[:type]=="orig"
      path = Rails.root.join('files', att.post.id.to_s, att.orig_name.to_s)
    elsif params[:type]=="thumb"
      path = Rails.root.join('files', att.post.id.to_s, 'thumb_'+att.orig_name.to_s)
    end
    if att.mime.start_with? "image"
      send_file path, type: att.mime, disposition: "inline"
    elsif att.mime == "audio/x-mpegurl"
      send_file Rails.root.join('files', att.post.id.to_s, att.orig_name.to_s), type: att.mime, disposition: "inline"
    else
      send_file Rails.root.join('files', att.post.id.to_s, att.orig_name.to_s), type: att.mime, disposition: "inline"
    end
  end
  
  def toggle_thumb
    #@attachment = Post.find(params[:id]).attachment
    #@id = params[:id]
    
    #respond_to do |format|
      #format.html {redirect_to @attachment.url(:original) }
      #format.js
    #end
  end
  
  private
  
  def attachment_params
    params.require(:attachment).permit(:file)
  end
end
