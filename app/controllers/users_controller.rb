class UsersController < ApplicationController
  layout = 'application'
  
  def new
    @user = User.new
  end
  
  # We actually EDIT a user if he creates an actual account
  def create
    if cookies.signed[:uid] != ""
      user = @user
      # Checks
      if user.nil?
        raise "Cannot create user: no matching uid found. This is odd."
      end
      if !user.password.empty?
        redirect_to "/404" and return
      end
      user.uname = params[:uname]
      user.password = PasswordHash.create( params[:password] )
      if user.valid?
        user.save!
        redirect_to request.referer
      else
        @error = ""
        user.errors.each{|attr, msg| @error += "#{attr} #{msg}<br>" }
        render file: "error"
      end
    end
  end
  
  def show
    @logins = @user.logins.order(updated_at: :desc)
    @series = cookies.signed[:series]
  end
  
  def edit
    user = @user
    user.show_sidebar = params['user']['show_sidebar']
    user.stylesheet = params['user']['stylesheet']
    user.save_scroll = params['user']['save_scroll']
    user.hide_names = params['user']['hide_names']
    user.hide_region = params['user']['hide_region']
    user.autoload_media = params['user']['autoload_media']
    user.autoplay_gifs = params['user']['autoplay_gifs']
    user.post_noko = params['user']['post_noko']
    user.save_scroll = params['user']['save_scroll']
    user.locale = params['user']['locale']
    if user.valid?
      user.save!
      @success = "Settings saved."
      redirect_to request.referer and return
    else
      @error = "Failed to save settings. Something went wrong."
      render file: "error"
    end
  end
  
  def login
    username = params[:uname]
    password = params[:password]
    user = User.where(uname: username)
    access = false
    if user.count!=0
      u=user.first
      if PasswordHash.validate(password, u.password)
        access = true
        l = Login.new
        user_agent = UserAgent.parse(request.user_agent)
        l.user = u
        l.browser = user_agent.browser+" "+user_agent.version+" ("+user_agent.platform+")"
        l.series = SecureRandom.hex
        l.token = SecureRandom.hex
        l.save!

        cookies.permanent.signed[:hb_token] = l.token
        cookies.permanent.signed[:hb_series] = l.series
        puts "Login success"
      end
    else
      puts "user count 0"
    end
    if access==false
      @error = "Wrong username or password!"
      render file: "error" and return
    end
    redirect_to "/"
  end
  
  def changepassword
    user = @user
    if PasswordHash.validate(params[:oldpass], user.password)
      if params[:newpass]===params[:newpass2]
        user.password = PasswordHash.create(params[:newpass])
        user.save!
        redirect_to request.referer
      end
    end
  end
  
  def logout
    # destroy associated login
    lgid = params[:id].to_i
    l = Login.find(lgid)
    if @user = l.user
      l.destroy
      redirect_to request.referer
    end
  end
  
  def edit2
    user = @user
    @previous_style = @user.stylesheet
    @user.stylesheet = params[:stylesheet] unless params[:stylesheet].nil?
    print @user.stylesheet
    @user.post_noko = params[:post_noko] unless params[:post_noko].nil?
    @user.locale = params[:locale] unless params[:locale].nil?
    @user.save_scroll = params[:save_scroll] unless params[:save_scroll].nil?
    @user.hide_names = params[:hide_names] unless params[:hide_names].nil?
    @user.hide_region = params[:hide_region] unless params[:hide_region].nil?
    @user.autoload_media = params[:autoload_media] unless params[:autoload_media].nil?
    @user.autoplay_gifs = params[:autoplay_gifs] unless params[:autoplay_gifs].nil?
    @user.save!
    render file: "stylesheet", layout: false
  end
end
