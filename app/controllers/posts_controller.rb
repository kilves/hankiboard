require 'taglib'
require 'zip'
class PostsController < ApplicationController
  layout = 'application'
  include CarrierWave::MiniMagick
  invisible_captcha only: [:create, :update]
  def list
  end
  
  def new
    @post = Post.new
  end

  def redirect
    post = Post.where(id: params[:id])
    src = params[:src]
    src.gsub!(/http(s*):\/\//, "")
    splits = src.split("/")
    if post.length==0
      render plain: "N"
    else
      post = post.first
      if post.thread_id == 0
        url = post.board.url+"/"+post.id.to_s+"#"+post.id.to_s
      else
        url = post.board.url+"/"+post.thread_id.to_s+"#"+post.id.to_s
      end
      if splits[1]==post.board && (splits[2] == post.id || splits[2] == post.thread_id)
        render plain "S"
      else
        render plain: "R "+url
      end
    end
  end
  
  def thread
    firstpost = Post.where(deleted_time: nil).where(id: params[:thread_id])
    if firstpost.length == 0
      @error = "No thread found"
      render file: "error" and return
    else
      firstpost = firstpost.first
    end
    ids = Array.new
    if firstpost.thread_id == 0
      ids.push(firstpost.id)
      hides = Hide.where(uid: @user.uid)
      posts = Post.where(thread_id: params[:thread_id]).where(deleted_time: nil)
      @thread = Array.new
      @thread.push(firstpost)
      posts.each do |p|
        ids.push(p.id)
        if !hides.any? { |h| h.post==p.id }
          @thread.push(p)
        end
      end
      @preloaded_attachments = Attachment.where(post_id: ids)
      render :showthread and return
    else
      redirect_to "/404"
    end
  end
  
  def show
    @post = Post.where(deleted_time: nil).find(params[:id])
    render file: "posts/show", layout: false
  end
  
  def hide
    hide = Hide.new
    hide.uid = @user.uid.to_s
    hide.post = params[:id]
    hide.time = DateTime.now
    hide.save!
    if Post.where(deleted_time: nil).find(params[:id]).thread_id==0
      @target = "Thread"
    else
      @target = "Post"
    end
    respond_to do |format|
      format.html { render file: "hide", layout: false }
    end
  end
  
  def unhide
    hide = Hide.where("uid = ? AND post = ?", @user.uid, params[:id])
    hide.destroy_all
    respond_to do |format|
      format.html { render nothing: true }
    end
  end
  
  def create
    # bancheck
    post = Post.create
    user = @user
    f = Post.where(uid: user.uid).order(created_at: :desc).first
    if params[:post][:thread_id]=="0" && params[:post][:message].empty?
      @error = "Empty new threads are not allowed."
      render file: "error" and return
    end
    if params[:post][:message].empty? && params[:post][:embed_code].empty? && params[:post][:attachments].empty?
      @error = "Completely empty messages are not allowed."
      render file: "error" and return
    end
    if !f.nil?
      if f.created_at > 20.seconds.ago && Rails.env == "production"
        @error = "You are only allowed to post every 20 seconds"
        render file: "error" and return
      end
    end
    t = Post.where(uid: user.uid).where(thread_id: 0).order(created_at: :desc).first
    if !t.nil? && params[:post][:thread_id]=="0"
      if t.created_at > 5.minutes.ago && Rails.env == "production"
        @error = "You are only allowed to create a new thread every 5 minutes."
        render file: "error" and return
      end
    end
    board = Board.find(params[:post][:board_id])
    thread = Post.find(params[:post][:thread_id]) if params[:post][:thread_id]!="0"
    if !thread.nil?
      if thread.locked
        @error = "Thread is locked"
        render file: "error" and return
      end
    end    
    if(user.is_banned_from(board.id))
      redirect_to "/#{board.url}/banned" and return
    end
    @error = Array.new
    if user.nil?
      raise "no user"
    end
    unless params[:post][:attachments].nil?
      if params[:post][:attachments].size>4
        @error = "You are only allowed to post a maximum of 4 images"
        render file: "error" and return
      end
    end
    post.attachments = params[:post][:attachments]
    post.board = Board.find(params[:post][:board_id])
    if post.board.user == user && params[:post][:mod]=="1"
      post.mod = true
    else
      post.mod = false
    end
    post.uid = @user.uid
    post.ip = Digest::SHA1.hexdigest(request.remote_ip+Rails.application.config.ip_salt)
    post.ip_plain=request.remote_ip
    g = GeoIP.new('GeoLiteCity.dat')
    g.local_ip_alias = "173.194.112.35"
    if post.board.international
      g = GeoIP.new('GeoLiteCity.dat')
      g.local_ip_alias = "173.194.112.35"
      geoip_data = g.city(request.remote_ip)
      post.geoip_country_code = geoip_data.country_code2.downcase
      post.geoip_country_name = geoip_data.country_name
      post.geoip_city = geoip_data.city_name
      post.geoip_region_code = geoip_data.region_name
      post.geoip_region_name = geoip_data.real_region_name
      post.geoip_lat = geoip_data.latitude
      post.geoip_lon = geoip_data.longitude
    else
      if Rails.env == "production" && g.city(request.remote_ip).country_code2.downcase != "fi"
        @error = "Posting to non-international boards outside finland is not allowed."
        render file: "error"
      end
    end

    if !params[:post][:embed_code].nil?
      if !params[:post][:embed_code].empty?
        post.embed_code = params[:post][:embed_code]
        post.embed_source = params[:post][:embed_source]
      end
    end
    if params[:post][:thread_id]=="0" && params[:post][:posted_by_op]=="1"
      post.posted_by_op = true
    elsif !thread.nil?
      if thread.uid==user.uid && params[:post][:posted_by_op]=="1"
        post.posted_by_op = true
      end
    else
      post.posted_by_op = false
    end
    post.time = DateTime.now
    if !params[:post][:name].nil?
      nametrip = params[:post][:name].split("#")
      if post.board.namefield == 2
        if nametrip.length == 1
          post.name = nametrip[0]
        elsif nametrip.length == 2
          seed = nametrip[1][1, 2]
          post.name = nametrip[0]+"!"+nametrip[1].crypt(seed)[-10, 10]
        end
      elsif post.board.namefield == 1
        if !nametrip[0].nil?
          if nametrip[0].empty?
            seed = nametrip[1][1, 2]
            post.name = "!"+nametrip[1].crypt(seed)[-10, 10]
          else
            @error.push "Name must only contain tripcodes here"
          end
        else
          post.name = ""
        end
      else
        if !params[:post][:name].empty?
          @error.push "Name must be empty on this board"
        end
      end
    else
      post.name = ""
    end
    post.subject = params[:post][:subject]
    post.message = params[:post][:message]
    post.message = post.formatMessage
    post.locked = false
    post.sticky = false
    post.sage = {"1" => true, "0" => false}[params[:post][:sage]]
    post.rage = params[:post][:rage]
    post.love = params[:post][:love]
    post.thread_id = params[:post][:thread_id]
    if post.thread_id == 0
      post.bump_time = DateTime.current
    elsif post.sage == false
      thread = Post.find(post.thread_id)
      thread.bump_time = DateTime.current
      thread.save
    end
    if post.valid? && @error.empty?
      post.save!
      board.update(postcount: board.posts.where(deleted_time: nil).count())
      # Attachment handling
      post.attachments.each do |a|
        attachment = Attachment.new
        attachment.post = post
        attachment.orig_name = a.url.split("/")[-1]
        attachment.extension = a.url.split("/")[-1].split(".")[-1]
        attachment.thumb_ext = attachment.extension
        attachment.mime = a.content_type
        attachment.size = a.file.size
        if a.content_type.start_with? "image"
          inf = ::MiniMagick::Image.open(a.file.file)[:dimensions]
          infth = ::MiniMagick::Image.open(a.thumb.file.file)[:dimensions]
        else
          
        end
        hash = Digest::SHA2.file(a.file.file).hexdigest
        dupe = Attachment.find_by(md5: hash)
        unless dupe.nil?
          # found a duplicate, delete current attachment and link it
          opath = Rails.root.join("files", post.id.to_s, attachment.orig_name)
          dpath = Rails.root.join("files", dupe.post.id.to_s, dupe.orig_name)
          if  a.content_type.start_with? "image"
            othumb = Rails.root.join("files", post.id.to_s, "thumb_"+attachment.orig_name)
            dthumb = Rails.root.join("files", dupe.post.id.to_s, "thumb_"+dupe.orig_name)
          end
          
          File.delete(opath)
          File.delete(othumb) if a.content_type.start_with? "image"
          File.link(dpath, opath)
          File.link(dthumb, othumb) if a.content_type.start_with? "image"
          attachment.duplicate_of = dupe.id
        end
        attachment.md5 = hash
        if a.content_type.start_with? "image"
          attachment.information = inf[0].to_s+"x"+inf[1].to_s
          attachment.thumb_width = infth[0]
          attachment.thumb_height = infth[1]
        elsif a.content_type.end_with? "webm"
        
        else
          TagLib::MPEG::File.open(a.file.file) do |f|
            attachment.id3_name = f.id3v2_tag.title
            attachment.id3_artist = f.id3v2_tag.artist
            attachment.id3_length = f.audio_properties.length.to_s
            attachment.id3_bitrate = f.audio_properties.bitrate.to_s
          end
        end
        attachment.name = Time.now.to_i.to_s+Random.rand(100000).to_s
        attachment.save!
      end
      
      # Add to replied threads if not already there
      value=false
      user.replied.post_ids.each do |i|
        if i.pid.to_s==params[:post][:thread_id].to_s
          value=true
        end
      end
      puts value.to_s
      if value==false && params[:post][:thread_id] != "0"
        if params[:post][:thread_id].to_i != 0
          user.replied.post_ids << PostId.new(:pid => post.thread_id)
        else
          user.replied.post_ids << PostId.new(:pid => post.id)
        end
        user.save!
      end
      
      
      # Redirect
      # Override all settings if noko was enabled in post
      if params[:post][:noko].to_i == 1
        redirect_to "/#{board.url}/#{post.thread_id}"
      elsif user.post_noko==0 || user.post_noko.nil?
        redirect_to "/#{board.url}"
      elsif user.post_noko==1
        redirect_to "/#{board.url}/#{post.thread_id}"
      elsif user.post_noko==2
        redirect_to "/ukko"
      end
    else
      raise post.errors.full_messages.join(", ")
    end
  end
  
  def delete
    postids=params[:posts].split("-")
    user = @user
    action = postids.shift
    postids.map!{|p| p.to_i}
    posts = Post.where(id: postids)

    if action=="delete"
      posts.each do |p|
        if user.perm("delete_posts", p.board.url) || @user.uid==p.uid
          p.deleted_time = DateTime.current
          p.save!
        end
      end
      redirect_to request.referer and return
    elsif action=="download"
      if !params[:filename].nil?
        filename = params[:filename]+".zip"
        if params[:filename].empty?
          filename = "images-#{Time.now.to_i.to_s}.zip"
        end
      else
        filename = "images-#{Time.now.to_i.to_s}.zip"
      end
      temp_file = Tempfile.new(filename)
      
      begin
        Zip::OutputStream.open(temp_file) {|zos|}
        Zip::File.open(temp_file.path, Zip::File::CREATE) do |zip|
          posts.each do |p|
            p.attachments.each do |a|
              zip.add(a.file.file.split("/")[-1], a.file.file)
            end
          end
        end
        
        data = File.read(temp_file.path)
        send_data(data, :type => "application/zip", :filename => filename)
      ensure
        temp_file.close
        temp_file.unlink
      end
      return
    else
      @error = "Malformed request"
      render file: "error"
    end
  end
  
  def update
    
  end
  
  def stick
    user = @user
    post = Post.find(params[:id])
    if(user.perm("stick_threads", post.board.url))
      post.sticky = true
      post.save!
    else
      @error = "You don't have permission to stick this thread"
      render file: "error" and return
    end
    redirect_to request.referer
  end
  
  def unstick
    user = @user
    post = Post.find(params[:id])
    if(user.perm("stick_threads", post.board.url))
      post.sticky = false
      post.save!
    else
      @error = "You don't have permission to unstick this thread"
      render file: "error" and return
    end
    redirect_to request.referer
  end
  
  def lock
    user = @user
    post = Post.find(params[:id])
    if(user.perm("lock_threads", post.board.url))
      post.locked = true
      post.save!
    else
      @error = "You don't have permission to lock this thread"
      render file: "error" and return
    end
    redirect_to request.referer
  end
  
  def unlock
    user = @user
    post = Post.find(params[:id])
    if(user.perm("lock_threads", post.board.url))
      post.locked = false
      post.save!
    else
      @error = "You don't have permission to unlock this thread"
      render file: "error" and return
    end
    redirect_to request.referer
  end
end
