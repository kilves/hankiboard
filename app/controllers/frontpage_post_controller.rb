class FrontpagePostController < ApplicationController
  def create
    user = @user
    if(user.has_perm("Admin"))
      post = FrontpagePost.new
      post.subject = params[:frontpage_post][:subject]
      post.text = params[:frontpage_post][:text]
      post.user = user
      post.save!
    end
    redirect_to "/"
  end
end
