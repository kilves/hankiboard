class ChatController < WebsocketRails::BaseController
  def initialize_session
    controller_store[:message_count] = 0
    user = @user
    user.in_chat = true
    puts "ok"
    user.chat_last = DateTime.current
    user.save
  end

  def new
    puts "ok"
    if(message[:message].length>=500)
      return
    end
    user = @user
    sd = ActionView::Base.full_sanitizer.sanitize(message[:message])
    if(user.password!="")
      msg = { :message => sd, :uname => user.uname }
    else
      msg = {:message => sd, :uname => user.uname[0, 10]}
    end
    broadcast_message "message.new", msg
  end

  def list
    WebsocketRails.users.each do |u|
      puts u
    end
    user = @user
    user.in_chat = true
    user.chat_last = DateTime.current
    users = User.where(in_chat: true)
    message = { "online" => "" }
    users.each do |u|
      if(u.chat_last<7.seconds.ago)
        u.chat_last=nil
        u.in_chat=false
        u.save
      end
      if(u.password.empty?)
        message["online"] = message["online"].split(":").push(u.uname[0, 10]).join(":")
      else
        message["online"] = message["online"].split(":").push(u.uname).join(":")
      end
    end
    send_message("userlist", message)
    user.save
  end

  def asd
    puts "OK"
  end
end
