class ReportController < ApplicationController
  def create
    report = Report.new
    report.reason = params[:reason]
    report.description = params[:description]
    report.post = Post.find(params[:id].to_i)
    report.board = report.post.board
    report.checked = false
    if report.valid?
      report.save!
      redirect_to request.referer
    else
      @error = report.errors.messages
      render file: "error"
    end
  end
  
  def check
    user = @user
    report = Report.find(params[:id])
    if user.perm("check_reports", report.board.url)
      report.checked = true
      report.save!
      redirect_to request.referer
    else
      @error = "You don't have the permission to check reports"
      render file: "error"
    end
  end
  
  def list
    user = @user
    boards = Array.new
    BoardPermission.where(uid: @user.uid).each do |p|
      if p.permission["check_reports"]
        boards.push p.board
      end
    end
    
    @reports = Array.new
    boards.each do |b|
      b.reports.where(checked: false).each do |r|
        @reports.push(r)
      end
    end
  end
end
