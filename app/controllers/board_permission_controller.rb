class BoardPermissionController < ApplicationController
  def create
    user = @user
    board = Board.find(params[:board])
    if(board.user == user)
      permvalue = Array.new
      user_to_add = User.find_by(uname: params[:username])
      board.board_permissions.where(uid: user_to_add.uid).destroy_all
      permission = BoardPermission.new
      permission.uid = user_to_add.uid
      permission.board = board
      ban = false
      unban = false
      check_reports = false
      change_settings = false
      change_rules = false
      stick_threads = false
      lock_threads = false
      delete_posts = false
      if !params[:can_ban].nil?
        ban = true
      end
      if !params[:can_unban].nil?
        unban = true
      end
      if !params[:can_check_reports].nil?
        check_reports = true
      end
      if !params[:can_change_settings].nil?
        change_settings = true
      end
      if !params[:can_change_rules].nil?
        change_rules = true
      end
      if !params[:can_stick_threads].nil?
        stick_threads = true
      end
      if !params[:can_lock_threads].nil?
        lock_threads = true
      end
      if !params[:can_delete_posts].nil?
        delete_posts = true
      end
      permission.permission = {"ban" => ban, "unban" => unban, "check_reports" => check_reports, "change_settings" => change_settings, "change_rules" => change_rules, "stick_threads" => stick_threads, "lock_threads" => lock_threads, "delete_posts" => delete_posts}
      puts permission.permission
      permission.save!
      if !permission.valid?
        raise permission.errors
      end
    else
      @error = "You are not the board owner, so you cannot change permissions."
      render file: "error"
    end
    redirect_to "/"+board.url+"/mod"
  end
end
