module BoardsHelper
  class String
    def shorten_path(len)
        arr = self.split(".")
        arr[0] = arr[0][0, len]+"[..]"
        return arr.join(".")
    end
  end
end
