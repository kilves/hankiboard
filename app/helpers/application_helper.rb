module ApplicationHelper
  def full_title(page_title = '')
      base_title = "Hanki board software"
      if page_title.empty?
          base_title
      else
          "#{page_title} | #{base_title}"
      end
  end
end

class String
  def shorten_path(len)
    return self unless self.length>len
    arr = self.split(".")
    arr = [arr[0, arr.length-1].join("."), arr.last]
    arr[0] = arr[0][0, len]+"[..]"
    return arr.join(".")
  end
  
  def my_strip(len, rep)
    if self.length>len
      return self[0, len-1]+rep.to_s
    end
    return self
  end
end
