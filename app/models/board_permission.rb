class BoardPermission < ActiveRecord::Base
  belongs_to :board
  serialize :permission
end
