class Ban < ActiveRecord::Base
  validates_presence_of :reason
  validates_presence_of :board
  validates_presence_of :expires
  belongs_to :user
end
