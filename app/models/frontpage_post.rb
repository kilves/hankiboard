class FrontpagePost < ActiveRecord::Base
  validates_presence_of :subject
  validates_presence_of :text
  belongs_to :user
end
