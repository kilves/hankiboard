class Permission < ActiveRecord::Base
  validates_presence_of :permission
  validates :permission, uniqueness: true
  belongs_to :user
end
