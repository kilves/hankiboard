class Post < ActiveRecord::Base
  #include Elasticsearch::Model
  #include Elasticsearch::Model::Callbacks

  belongs_to :board
  serialize :attachments
  has_many :reports
  mount_uploaders :attachments, AttachmentUploader
  has_many :attachments
  validates_presence_of :thread_id
  validates_numericality_of :board_id, :thread_id

  geocoded_by :ip_plain do |obj, results|
    if geo = results.first
      obj.geoip_lat = geo.latitude
      obj.geoip_lon = geo.longitude
      obj.geoip_country_code = geo.country_code
      obj.geoip_city = geo.city
    end
  end
  
  # returns an array in the following form:
  # 0: Starting post in thread
  # 1.. all posts in thread except starting post
  # returns nil if the given post is not a thread
  def to_thread(uid)
    if(self.thread_id == 0)
      hide = Hide.where(uid: uid.to_s).select(:post)
      @posts = Array.new
      posts = Post.where(thread_id: self.id).order(id: :asc)
      @posts.push(self)
      posts.each do |p|
        puts hide.any? { |h| h.post != p.id }
        if !hide.any? { |h| h.post == p.id }
          puts 'pushing '+p.to_s+' to posts'
          @posts.push(p)
        end
      end
      return @posts
    else
      return nil
    end
  end
  
  # Returns 'count' posts in thread starting from 'first'st post.
  def get_posts(first, count)
    if(self.thread_id == 0)
      @posts = Post.where(thread_id: self.id).order(id: :asc)
    else
      return nil
    end
  end
  
  # formats message to HTML.
  def formatSubject
    message = self.subject
    message.gsub!('&', '&amp;')
    message.gsub!('"', '&quot;')
    message.gsub!('\'', '&#39;')
    message.gsub!('`', '&#96;')
    message.gsub!(/\s{5,}/, ' ') #disallow too many spaces
    message.gsub!('!', '&#33;')
    message.gsub!('@', '&#64;')
    message.gsub!('$', '&#36;')
    message.gsub!('%', '&#37;')
    message.gsub!('(', '&#40;')
    message.gsub!(')', '&#41;')
    message.gsub!('=', '&#61;')
    message.gsub!('+', '&#43;')
    message.gsub!('{', '&#123;')
    message.gsub!('}', '&#125;')
    message.gsub!('[', '&#91;')
    message.gsub!(']', '&#93;')
    message.gsub!(/\n/, '&nbsp;')
    message.gsub!(/\s/, '&nbsp;')
    message.gsub!('<', '&lt;')
    message.gsub!('>', '&gt;')
    message.gsub!('[v]', ' ')
    message.gsub!('[', '<')
    message.gsub!(']', '>')
    return message
  end
  
  def formatMessage
    message = self.message
    message.gsub!('&', '&amp;')
    message.gsub!('"', '&quot;')
    message.gsub!('\'', '&#39;')
    message.gsub!('`', '&#96;')
    message.gsub!(/\s{5,}/, ' ') #disallow too many spaces
    message.gsub!('!', '&#33;')
    message.gsub!('@', '&#64;')
    message.gsub!('$', '&#36;')
    message.gsub!('%', '&#37;')
    message.gsub!('(', '&#40;')
    message.gsub!(')', '&#41;')
    message.gsub!('=', '&#61;')
    message.gsub!('+', '&#43;')
    message.gsub!('{', '&#123;')
    message.gsub!('}', '&#125;')
    message.gsub!(/\[b\](.+)\[\/b\]/, '(b)\1(/b)')
    message.gsub!(/\[i\](.+)\[\/i\]/, '(i)\1(/i)')
    message.gsub!(/\[u\](.+)\[\/u\]/, '(u)\1(/u)')
    message.gsub!(/\[spoiler\](.+)\[\/spoiler\]/, '(span class="spoiler")\1(/span)')
    message.gsub!('[', '&#91;')
    message.gsub!(']', '&#93;')
    message.gsub!(/\bhttps?:\/\/\S+/) {|s| s = '(a href="'+s+'")'+s+'(/a)'}
    message.gsub!(/^(>[^>].+)/, '(span class="greenquote")\1(/span)')
    message.gsub!(/^(<.+)/, '(span class="bluequote")\1(/span)')
    message.gsub!(/>>(\d+)/, '(span class="reference")(a href="/redirect/\1")>>\1(/a)(/span)')
    message.gsub!(/\n/, '(br)')
    message.gsub!(/\s/, ' ')
    message.gsub!('<', '&lt;')
    message.gsub!('>', '&gt;')
    message.gsub!('[v]', ' ')
    message.gsub!('(', '<')
    message.gsub!(')', '>')
    return message
  end
end
#Post.import
