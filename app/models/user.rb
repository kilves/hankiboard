class User < ActiveRecord::Base
  validates_presence_of :uid
  validates :uname, uniqueness: true, allow_blank: true
  
  has_one :overboard
  has_one :replied
  has_many :boards
  has_many :frontpage_posts
  has_many :permissions
  has_many :bans
  has_many :logins

  def has_perm(permission)
    return self.permissions.exists?(permission: permission)
  end
  
  def online_now
    if self.updated_at > 15.minutes.ago
      return true
    else
      return false
    end
  end
  
  def add_perm(permission)
    perm = Permission.new
    perm.permission = permission
    perm.user = self
    perm.save!
    if perm.valid?
      return true
    else
      return false
    end
  end
  
  def del_perm(permission)
    perm = self.permissions.find_by(permission: permission)
    if perm
      perm.delete
    end
  end
  
  def perm(permission, url)
    a = Board.find_by(url: url).board_permissions.find_by(uid: self.uid)
    if a.nil?
      return false
    end
    b = a.permission
    if b.nil?
      return false
    end
    return b[permission]
  end

  def dump_perms
    a = BoardPermission.where(uid: self.uid)
    c = Hash.new
    a.each do |b|
      puts b.permission
      c[b.board_id.to_s] = b.permission
    end
    return c
  end
  
  def ban_from(board, expires)
    b = Ban.new
    b.board = board
    b.expires = expires
    b.user = self
    if b.valid?
      b.save!
      return true
    end
  end
  
  def is_banned_from(board)
    ip = self.ip
    user_id = self.id
    bans = Ban.where("ip = ? OR user_id = ?", ip, user_id)
    bans.each do |b|
      return true if b.expires.future?
    end
    return false
  end
  
  def can_ban_from(board)
    perms = board.board_permissions.find_by(uid: self.uid)
    return false if perms.nil?
    return board.board_permissions.find_by(uid: self.uid).permission["ban"]
  end
end
