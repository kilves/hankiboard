class Replied < ActiveRecord::Base
  belongs_to :user
  has_many :post_ids
end
