class Board < ActiveRecord::Base
  has_many :posts
  has_many :board_permissions
  has_many :reports
  has_many :report_reasons
  belongs_to :category
  belongs_to :user
  has_and_belongs_to_many :overboards
  
  validates :name, presence: true,
  format: { without: /\W+/, message: "only allows letters, digits and underscores" },
  uniqueness: true
  
  validates :url, presence: true,
  format: { without: /\W+/, message: "only allows letters, digits and underscores" },
  uniqueness: true
  
  validates :description, presence: true
  validates :namefield, presence: true,
  format: { without: /[^012]{1}/, message: "only allows 0, 1, 2" }
  def clear_old_boards
    Board.all.each do |b|
      created = b.created_at
      last = b.posts.order(created_at: :desc).first
      time = [created, last].max
      if time < 1.month.ago
        b.posts.each do |p|
          p.deleted_at = DateTime.now
          p.save
        end
        b.destroy
      end
    end
  end
end
