Rails.application.routes.draw do
  get 'thread/new'

  get 'board_system/new'

  get 'posts/new'

  get 'boards/new'
  
  get 'posts/list'
  
  get 'attachments/show'
  
  get '/togglethumb/:id', to: 'attachments#toggle_thumb', constraints: { id: /\d+/ }
  get '/redirect/:id', to: 'application#redirect'
  get '/settings', to: 'users#show'
  get '/user/edit', to: 'users#edit2'
  get '/jsrdr', to: 'posts#redirect'
  post '/settings/createaccount', to: 'users#create'
  post '/settings/changepassword', to: 'users#changepassword'
  post '/login', to: 'users#login'
  post '/post', to: 'posts#create'
  post '/frontpagepost', to: 'frontpage_post#create'
  post '/perms/create', to: 'board_permission#create'
  post '/rules/create', to: 'report_reason#create'
  get '/logout/:id', to: 'users#logout'
  get '/expand', to: 'boards#expand'
  get '/hide_thread', to: 'posts#hide'
  get '/unhide', to: 'posts#unhide'
  get '/browse', to: 'boards#list'
  get '/subscribe/:id', to: 'overboard#subscribe'
  get '/unsubscribe', to: 'overboard#delete'
  get '/unsubscribe/:id', to: 'overboard#delete'
  get '/createboard', to: 'boards#create'
  get '/checkreport/:id', to: 'report#check'
  get '/reports', to: 'report#list'
  get '/:url/stick/:id', to: 'posts#stick'
  get '/:url/unstick/:id', to: 'posts#unstick'
  get '/:url/lock/:id', to: 'posts#lock'
  get '/:url/unlock/:id', to: 'posts#unlock'
  get '/unban/:id', to: 'ban#delete'
  post '/createboard', to: 'boards#create'
  post '/deleteposts', to: 'posts#delete'
  post '/report', to: 'report#create'
  get '/singlepost/:id', to: 'posts#show'
  get '/:url/mod', to: 'boards#update'
  get '/:url/delete', to: 'boards#confirm_delete'
  post '/:url/delete', to: 'boards#delete'
  get '/:url/ban/:ip/:id', to: 'ban#create_form'
  post '/:url/ban', to: 'ban#create'
  post '/:url/mod', to: 'boards#update'
  get '/:url/threadlist', to: 'boards#threadlist'
  get '/ukko/page/:page', to: 'overboard#show', constraints: { page: /\d+/ }
  get '/files/:type/:name/*save_as', to: 'attachments#show'
  patch '/settings/save', to: 'users#edit'
  get '/:url/:thread_id', to: 'posts#thread', constraints: { thread_id: /\d+/ }

  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  root 'application#frontpage'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end

Rails.application.routes.append do
  get '/:url', to: 'boards#show'
end
