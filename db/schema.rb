# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20151225150740) do

  create_table "attachments", force: :cascade do |t|
    t.string   "orig_name",         limit: 255
    t.string   "name",              limit: 255
    t.string   "extension",         limit: 255
    t.string   "thumb_ext",         limit: 255
    t.integer  "thumb_width",       limit: 4
    t.integer  "thumb_height",      limit: 4
    t.string   "mime",              limit: 255
    t.integer  "size",              limit: 4
    t.string   "information",       limit: 255
    t.string   "md5",               limit: 255
    t.integer  "duplicate_of",      limit: 4
    t.text     "id3_name",          limit: 65535
    t.text     "id3_artist",        limit: 65535
    t.string   "id3_length",        limit: 255
    t.string   "id3_bitrate",       limit: 255
    t.integer  "id3_image",         limit: 4
    t.string   "folder",            limit: 255
    t.datetime "created_at",                      null: false
    t.datetime "updated_at",                      null: false
    t.text     "file_meta",         limit: 65535
    t.string   "file_file_name",    limit: 255
    t.string   "file_content_type", limit: 255
    t.integer  "file_file_size",    limit: 4
    t.datetime "file_updated_at"
    t.integer  "post_id",           limit: 4
  end

  add_index "attachments", ["post_id"], name: "index_attachments_on_post_id"

  create_table "attachments_posts", id: false, force: :cascade do |t|
    t.integer "attachment_id", limit: 4
    t.integer "post_id",       limit: 4
  end

  add_index "attachments_posts", ["attachment_id"], name: "index_attachments_posts_on_attachment_id"
  add_index "attachments_posts", ["post_id"], name: "index_attachments_posts_on_post_id"

  create_table "bans", force: :cascade do |t|
    t.integer  "board",      limit: 4
    t.datetime "expires"
    t.integer  "user_id",    limit: 4
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
    t.integer  "post_id",    limit: 4
    t.text     "ip",         limit: 65535
    t.string   "ip_plain",   limit: 255
    t.text     "reason",     limit: 65535
    t.text     "banned_by",  limit: 65535
    t.text     "note",       limit: 65535
  end

  add_index "bans", ["user_id"], name: "index_bans_on_user_id"

  create_table "board_permissions", force: :cascade do |t|
    t.string   "uid",        limit: 255
    t.text     "permission", limit: 65535
    t.integer  "board_id",   limit: 4
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
  end

  add_index "board_permissions", ["board_id"], name: "index_board_permissions_on_board_id"

  create_table "boards", force: :cascade do |t|
    t.integer  "ident",            limit: 4
    t.string   "url",              limit: 255
    t.string   "name",             limit: 255
    t.string   "description",      limit: 255
    t.integer  "order",            limit: 4
    t.boolean  "international",    limit: 1
    t.integer  "pages",            limit: 4
    t.boolean  "locked",           limit: 1
    t.boolean  "worksafe",         limit: 1
    t.integer  "ad_category",      limit: 4
    t.integer  "namefield",        limit: 4
    t.text     "default_name",     limit: 65535
    t.boolean  "show_empty_names", limit: 1
    t.text     "default_style",    limit: 65535
    t.boolean  "hide_sidebar",     limit: 1
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
    t.integer  "category_id",      limit: 4
    t.integer  "user_id",          limit: 4
    t.boolean  "hidden",           limit: 1
    t.integer  "postcount",        limit: 4
  end

  add_index "boards", ["user_id"], name: "index_boards_on_user_id"

  create_table "boards_overboards", id: false, force: :cascade do |t|
    t.integer "board_id",     limit: 4
    t.integer "overboard_id", limit: 4
  end

  add_index "boards_overboards", ["board_id"], name: "index_boards_overboards_on_board_id"
  add_index "boards_overboards", ["overboard_id"], name: "index_boards_overboards_on_overboard_id"

  create_table "categories", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.integer  "order",      limit: 4
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "embed_sources", force: :cascade do |t|
    t.string  "name",       limit: 255
    t.text    "video_url",  limit: 65535
    t.text    "embed_code", limit: 65535
    t.integer "width",      limit: 4
    t.integer "height",     limit: 4
    t.text    "code",       limit: 65535
    t.text    "help",       limit: 65535
    t.boolean "sfw",        limit: 1
  end

  create_table "frontpage_posts", force: :cascade do |t|
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
    t.text     "subject",    limit: 65535
    t.text     "text",       limit: 65535
    t.integer  "user_id",    limit: 4
  end

  add_index "frontpage_posts", ["user_id"], name: "index_frontpage_posts_on_user_id"

  create_table "hides", force: :cascade do |t|
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
    t.integer  "post",       limit: 4
    t.datetime "time"
    t.string   "uid",        limit: 255
  end

  create_table "logins", force: :cascade do |t|
    t.integer  "user_id"
    t.string   "series"
    t.string   "token"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.text     "browser"
  end

  add_index "logins", ["user_id"], name: "index_logins_on_user_id"

  create_table "overboards", force: :cascade do |t|
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
    t.integer  "user_id",    limit: 4
    t.text     "boards",     limit: 65535
  end

  create_table "permissions", force: :cascade do |t|
    t.text     "permission", limit: 65535
    t.integer  "user_id",    limit: 4
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
  end

  add_index "permissions", ["user_id"], name: "index_permissions_on_user_id"

  create_table "post_ids", force: :cascade do |t|
    t.integer  "pid",        limit: 4
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
    t.integer  "replied_id", limit: 4
  end

  create_table "posts", force: :cascade do |t|
    t.string   "uid",                limit: 255
    t.string   "ip",                 limit: 255
    t.string   "ip_plain",           limit: 255
    t.boolean  "proxy",              limit: 1
    t.string   "geoip_country_code", limit: 255
    t.text     "geoip_country_name", limit: 16777215
    t.string   "geoip_region_code",  limit: 255
    t.text     "geoip_region_name",  limit: 16777215
    t.text     "geoip_city",         limit: 16777215
    t.text     "geoip_lat",          limit: 16777215
    t.text     "geoip_lon",          limit: 16777215
    t.string   "name",               limit: 255
    t.string   "tripcode",           limit: 255
    t.boolean  "posted_by_op",       limit: 1
    t.boolean  "modpost",            limit: 1
    t.datetime "time"
    t.string   "subject",            limit: 255
    t.text     "message",            limit: 16777215
    t.string   "password",           limit: 255
    t.integer  "embed_source",       limit: 4
    t.text     "embed_code",         limit: 16777215
    t.boolean  "sage",               limit: 1
    t.boolean  "rage",               limit: 1
    t.boolean  "love",               limit: 1
    t.datetime "bump_time"
    t.boolean  "locked",             limit: 1
    t.boolean  "sticky",             limit: 1
    t.datetime "deleted_time"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.integer  "thread_id",          limit: 4
    t.integer  "board_id",           limit: 4
    t.boolean  "noko",               limit: 1
    t.text     "attachments",        limit: 16777215
    t.boolean  "mod",                limit: 1
    t.integer  "embed_source_id",    limit: 4
    t.text     "modinfo",            limit: 16777215
  end

  add_index "posts", ["board_id"], name: "index_posts_on_board_id"
  add_index "posts", ["thread_id"], name: "index_posts_on_thread_id"

  create_table "posts_replieds", id: false, force: :cascade do |t|
    t.integer "post_id",    limit: 4
    t.integer "replied_id", limit: 4
  end

  add_index "posts_replieds", ["post_id"], name: "index_posts_replieds_on_post_id"
  add_index "posts_replieds", ["replied_id"], name: "index_posts_replieds_on_replied_id"

  create_table "replieds", force: :cascade do |t|
    t.integer  "user_id",    limit: 4
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
  end

  create_table "report_reasons", force: :cascade do |t|
    t.text     "name",        limit: 65535
    t.text     "description", limit: 65535
    t.integer  "board_id",    limit: 4
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
  end

  add_index "report_reasons", ["board_id"], name: "index_report_reasons_on_board_id"

  create_table "reports", force: :cascade do |t|
    t.integer  "reason",      limit: 4
    t.text     "description", limit: 65535
    t.integer  "board_id",    limit: 4
    t.integer  "post_id",     limit: 4
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
    t.boolean  "checked",     limit: 1
  end

  add_index "reports", ["board_id"], name: "index_reports_on_board_id"
  add_index "reports", ["post_id"], name: "index_reports_on_post_id"

  create_table "threads", force: :cascade do |t|
    t.integer "board_id"
  end

  add_index "threads", ["board_id"], name: "index_threads_on_board_id"

  create_table "users", force: :cascade do |t|
    t.string   "uid",                 limit: 255
    t.string   "ip",                  limit: 255
    t.boolean  "online",              limit: 1
    t.datetime "last_load"
    t.string   "last_page",           limit: 255
    t.text     "site_style",          limit: 65535
    t.integer  "show_sidebar",        limit: 4
    t.boolean  "show_postform",       limit: 1
    t.boolean  "save_scroll",         limit: 1
    t.boolean  "sfw",                 limit: 1
    t.text     "locale",              limit: 65535
    t.text     "timezone",            limit: 65535
    t.text     "post_password",       limit: 65535
    t.string   "post_name",           limit: 255
    t.integer  "post_noko",           limit: 4
    t.boolean  "hide_names",          limit: 1
    t.boolean  "autoload_media",      limit: 1
    t.boolean  "autoplay_gifs",       limit: 1
    t.boolean  "hide_region",         limit: 1
    t.integer  "hide_ads",            limit: 4
    t.boolean  "hide_browserwarning", limit: 1
    t.string   "uname",               limit: 255
    t.string   "password",            limit: 255
    t.datetime "created_at",                        null: false
    t.datetime "updated_at",                        null: false
    t.string   "password_salt",       limit: 255
    t.string   "token",               limit: 255
    t.string   "series",              limit: 255
    t.text     "stylesheet",          limit: 65535
    t.datetime "chat_last"
    t.boolean  "in_chat",             limit: 1
  end

end
