class UpdatePosts < ActiveRecord::Migration
  def change
    remove_column :posts, :avatar_meta, :text
    add_column :posts, :attachment_meta, :text
    remove_column :posts, :dimensions, :string
  end
end
