class AddPostIdToFileEmbeds < ActiveRecord::Migration
  def change
    add_column :file_embeds, :post_id, :integer
    add_index :file_embeds, :post_id
  end
end
