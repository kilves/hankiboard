class CreateLogins < ActiveRecord::Migration
  def change
    create_table :logins do |t|
      t.belongs_to :user, index: true
      t.string :series
      t.string :token
      t.timestamps null: false
    end
  end
end
