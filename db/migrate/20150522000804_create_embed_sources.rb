class CreateEmbedSources < ActiveRecord::Migration
  def change
    create_table :embed_sources do |t|
      t.string :name
      t.text :video_url
      t.text :embed_code
      t.integer :width
      t.integer :height
      t.text :code
      t.text :help
      t.boolean :sfw
    end
  end
end
