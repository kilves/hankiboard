class AddDimensionsToPost < ActiveRecord::Migration
  def change
    add_column :posts, :dimensions, :string
  end
end
