class AddStylesheetToUsers < ActiveRecord::Migration
  def change
    change_table :users do |t|
      t.text "stylesheet"
    end
  end
end
