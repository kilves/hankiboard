class EditPostsAndAttachments < ActiveRecord::Migration
  def change
  
    change_table :posts do |t|
      t.remove :attachment_file_name
      t.remove :attachment_content_type
      t.remove :attachment_file_size
      t.remove :attachment_updated_at
      t.remove :attachment_meta
    end
    
    change_table :attachments do |t|
      t.remove :post_id
      t.text :file_meta
    end
    
    create_table :attachments_posts, id: false do |t|
      t.belongs_to :attachment, index: true
      t.belongs_to :post, index: true
    end
  end
end
