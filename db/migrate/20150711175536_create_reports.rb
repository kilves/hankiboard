class CreateReports < ActiveRecord::Migration
  def change
    create_table :reports do |t|
      t.integer :reason
      t.text :description
      t.belongs_to :board, index: true
      t.belongs_to :post, index: true
      t.timestamps null: false
    end
  end
end
