class RemovePostIdFromFileEmbeds < ActiveRecord::Migration
  def change
    remove_column :file_embeds, :post_id, :integer
  end
end
