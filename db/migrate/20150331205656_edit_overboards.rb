class EditOverboards < ActiveRecord::Migration
  def change
    change_table :overboards do |t|
      t.integer :user_id
    end
  end
end
