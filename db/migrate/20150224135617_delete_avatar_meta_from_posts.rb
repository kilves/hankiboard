class DeleteAvatarMetaFromPosts < ActiveRecord::Migration
  def change
    delete_column :posts, :avatar_meta, :text
    add_colum :posts, :attachment_meta, :text
    delete_column :posts, :dimensions, :string
  end
end
