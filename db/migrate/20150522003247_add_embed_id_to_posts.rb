class AddEmbedIdToPosts < ActiveRecord::Migration
  def change
    change_table :posts do |t|
      t.integer :embed_source_id
    end
  end
end
