class CombineFileEmbedsWithAttachments < ActiveRecord::Migration
  def change
    drop_table :attachments
    rename_table :file_embeds, :attachments
  end
end
