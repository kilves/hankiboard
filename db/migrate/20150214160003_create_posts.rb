class CreatePosts < ActiveRecord::Migration
  def change
    create_table :posts do |t|
      t.integer :ident
      t.integer :board
      t.integer :thread
      t.string :uid
      t.string :ip
      t.string :ip_plain
      t.boolean :proxy
      t.string :geoip_country_code
      t.text :geoip_country_name
      t.string :geoip_region_code
      t.text :geoip_region_name
      t.text :geoip_city
      t.text :geoip_lat
      t.text :geoip_lon
      t.string :name
      t.string :tripcode
      t.boolean :posted_by_op
      t.boolean :modpost
      t.datetime :time
      t.string :subject
      t.text :message
      t.string :password
      t.integer :embed_source
      t.text :embed_code
      t.boolean :sage
      t.boolean :rage
      t.boolean :love
      t.datetime :bump_time
      t.boolean :locked
      t.boolean :sticky
      t.datetime :deleted_time

      t.timestamps null: false
    end
  end
end
