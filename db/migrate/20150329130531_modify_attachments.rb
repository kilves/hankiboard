class ModifyAttachments < ActiveRecord::Migration
  def change
    change_table :posts do |t|
      t.remove :attachment
    end
    
    change_table :attachments do |t|
      t.belongs_to :post, index: true
    end
  end
end
