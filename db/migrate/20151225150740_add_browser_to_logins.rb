class AddBrowserToLogins < ActiveRecord::Migration
  def change
    change_table :logins do |t|
      t.text :browser
    end
  end
end
