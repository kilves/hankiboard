class CreateFileEmbeds < ActiveRecord::Migration
  def change
    create_table :file_embeds do |t|
      t.string :orig_name
      t.string :name
      t.string :extension
      t.string :thumb_ext
      t.integer :thumb_width
      t.integer :thumb_height
      t.string :mime
      t.integer :size
      t.string :information
      t.string :md5
      t.integer :duplicate_of
      t.text :id3_name
      t.text :id3_artist
      t.string :id3_length
      t.string :id3_bitrate
      t.integer :id3_image
      t.string :folder

      t.timestamps null: false
    end
  end
end
