class AddAvatarMetaToPosts < ActiveRecord::Migration
  def change
    add_column :posts, :avatar_meta, :text
  end
end
