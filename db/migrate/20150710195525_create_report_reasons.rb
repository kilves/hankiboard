class CreateReportReasons < ActiveRecord::Migration
  def change
    create_table :report_reasons do |t|
      t.text :name
      t.text :description
      t.belongs_to :board, index: true
      t.timestamps null: false
    end
  end
end
