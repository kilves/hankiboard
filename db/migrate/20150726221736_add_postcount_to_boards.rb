class AddPostcountToBoards < ActiveRecord::Migration
  def change
    change_table :boards do |t|
      t.integer :postcount
    end
  end
end
