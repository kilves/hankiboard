class AddStuffToBans < ActiveRecord::Migration
  def change
    change_table :bans do |t|
      t.integer :post_id
      t.text :ip
      t.string :ip_plain
      t.text :reason
    end
  end
end
