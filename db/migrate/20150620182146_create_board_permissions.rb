class CreateBoardPermissions < ActiveRecord::Migration
  def change
    create_table :board_permissions do |t|
      t.string :uid
      t.text :permission
      t.belongs_to :board, index: true
      t.timestamps null: false
    end
  end
end
