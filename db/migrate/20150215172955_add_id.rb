class AddId < ActiveRecord::Migration
  def change
    remove_column :posts, :thread_id
    change_table :posts do |p|
      p.integer :thread_id
    end
    
    create_table :threads do |t|
      t.integer :board_id
    end
    add_index :posts, :thread_id
    add_index :threads, :board_id
  end
end
