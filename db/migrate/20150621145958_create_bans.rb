class CreateBans < ActiveRecord::Migration
  def change
    create_table :bans do |t|
      t.integer :board
      t.datetime :expires
      t.belongs_to :user, index: true
      t.timestamps null: false
    end
  end
end
