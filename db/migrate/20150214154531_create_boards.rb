class CreateBoards < ActiveRecord::Migration
  def change
    create_table :boards do |t|
      t.integer :ident
      t.string :url
      t.string :name
      t.string :description
      t.integer :order
      t.integer :category
      t.boolean :international
      t.integer :pages
      t.boolean :locked
      t.boolean :worksafe
      t.integer :ad_category
      t.integer :namefield
      t.text :default_name
      t.boolean :show_empty_names
      t.text :default_style
      t.boolean :hide_sidebar

      t.timestamps null: false
    end
  end
end
