class ForignKeys < ActiveRecord::Migration
  def change
    change_table :posts do |p|
      p.integer :board_id
    end
    
    add_index :posts, :board_id
  end
end
