class RemoveIdentFromPosts < ActiveRecord::Migration
  def change
    remove_column :posts, :ident, :integer
  end
end
