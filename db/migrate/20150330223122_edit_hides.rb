class EditHides < ActiveRecord::Migration
  def change
    change_table :hides do |t|
      t.remove :time
      t.datetime :time
    end
  end
end
