class RemoveUnnecessities < ActiveRecord::Migration
  def change
    change_table :posts do |t|
      t.remove :board
    end
  end
end
