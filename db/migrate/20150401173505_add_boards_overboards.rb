class AddBoardsOverboards < ActiveRecord::Migration
  def change
    create_table :boards_overboards, id: false do |t|
      t.belongs_to :board, index: true
      t.belongs_to :overboard, index: true
    end
  end
end
