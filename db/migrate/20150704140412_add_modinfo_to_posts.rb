class AddModinfoToPosts < ActiveRecord::Migration
  def change
    change_table :posts do |t|
      t.text :modinfo
    end
  end
end
