class AddCheckedToReports < ActiveRecord::Migration
  def change
    change_table :reports do |t|
      t.boolean :checked
    end
  end
end
