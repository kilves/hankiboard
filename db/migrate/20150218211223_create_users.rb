class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :uid
      t.string :ip
      t.boolean :online
      t.datetime :last_load
      t.string :last_page
      t.text :site_style
      t.integer :show_sidebar
      t.boolean :show_postform
      t.boolean :save_scroll
      t.boolean :sfw
      t.text :locale
      t.text :timezone
      t.text :post_password
      t.string :post_name
      t.boolean :post_noko
      t.boolean :hide_names
      t.boolean :autoload_media
      t.boolean :autoplay_gifs
      t.boolean :hide_region
      t.integer :hide_ads
      t.boolean :hide_browserwarning
      t.string :uname
      t.string :password

      t.timestamps null: false
    end
  end
end
