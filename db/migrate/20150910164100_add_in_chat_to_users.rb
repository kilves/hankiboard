class AddInChatToUsers < ActiveRecord::Migration
  def change
    add_column :users, :in_chat, :boolean
  end
end
