class CreateHides < ActiveRecord::Migration
  def change
    create_table(:hides, index: false) do |t|
      t.timestamps null: false
      t.integer :uid
      t.integer :post
      t.timestamp :time
    end
  end
end
