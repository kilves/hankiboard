class CreateOverboards < ActiveRecord::Migration
  def change
    create_table :overboards do |t|
      t.string :uid
      t.timestamps null: false
    end
  end
end
