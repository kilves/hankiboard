class AddModToPosts < ActiveRecord::Migration
  def change
    change_table :posts do |t|
      t.boolean :mod
    end
  end
end
