class CreateReplieds < ActiveRecord::Migration
  def change
    create_table :replieds do |t|
      t.integer :user_id
      t.timestamps null: false
    end
    create_table :replieds_boards, id: false do |t|
      t.belongs_to :board, index: true
      t.belongs_to :replied, index: true
    end
  end
end
