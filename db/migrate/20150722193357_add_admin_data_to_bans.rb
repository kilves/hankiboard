class AddAdminDataToBans < ActiveRecord::Migration
  def change
    change_table :bans do |t|
      t.text :banned_by
      t.text :note
    end
  end
end
