class EditStuff < ActiveRecord::Migration
  def change
    change_table :overboards do |t|
      t.remove :uid
      t.text :boards
    end
    drop_table :followedboards
    drop_table :attachment_containers
  end
end
