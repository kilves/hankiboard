class AddAttachmentsToPosts < ActiveRecord::Migration
  def change
    add_column :posts, :attachments, :text
  end
end
