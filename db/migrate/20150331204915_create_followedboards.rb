class CreateFollowedboards < ActiveRecord::Migration
  def change
    create_table :followedboards do |t|
      t.string :uid
      t.timestamps null: false
    end
  end
end
