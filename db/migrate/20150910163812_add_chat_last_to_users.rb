class AddChatLastToUsers < ActiveRecord::Migration
  def change
    change_table :users do |t|
      t.datetime :chat_last
    end 
  end
end
