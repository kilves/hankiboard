class AddUserToBoards < ActiveRecord::Migration
  def change
    change_table :boards do |t|
      t.belongs_to :user, index: true
    end
  end
end
