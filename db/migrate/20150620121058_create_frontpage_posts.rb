class CreateFrontpagePosts < ActiveRecord::Migration
  def change
    create_table :frontpage_posts do |t|
      t.timestamps null: false
      t.text :subject
      t.text :text
      t.belongs_to :user, index: true
    end
  end
end
