class EditReplieds < ActiveRecord::Migration
  def change
    drop_table :replieds_boards
    create_table :replieds_boards, id: false do |t|
      t.belongs_to :post, index: true
      t.belongs_to :replied, index: true
    end
    
  end
end
