class CreateAttachmentContainers < ActiveRecord::Migration
  def change
    create_table :attachment_containers do |t|

      t.timestamps null: false
    end
  end
end
