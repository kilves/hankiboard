class CreateCategories < ActiveRecord::Migration
  def change
    create_table :categories do |t|
      t.string :name
      t.integer :order
      t.timestamps null: false
    end
    change_table :boards do |t|
      t.integer :category_id
    end
  end
end
