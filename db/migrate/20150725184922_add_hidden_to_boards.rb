class AddHiddenToBoards < ActiveRecord::Migration
  def change
    change_table :boards do |t|
      t.boolean :hidden
    end
  end
end
