== README

Hanki board software
====================

Stuff
-----

### Ruby version

Developed with ruby 2.2.0, rails 4.2.0. If you are planning to run this with older rails versions you might want to change the gemfile a bit as the older versions may need different gems/dependencies.

### Installation

These instructions assume that you have debian wheezy, but I don't see a reason
why this wouldn't work on other distros with slight modifications. Note that as of now, these installation instructions are **only intended for development usage** and not for production. For production, you might want to replace rails server with something like nginx-unicorn combo.

1. Make sure that your repositories are up-to-date:
`apt-get update`

2. Install the dependencies:
`apt-get install curl git mysql-client mysql-server elasticsearch libmysqlclient-dev libtag1-dev libpq-dev libz-dev libpng12-dev libglib2.0-dev zlib1g-dev libbz2-dev libtiff-dev libjpeg-dev imagemagick`

3. ~~Hankiboard uses elasticsearch as a search engine so you'll need to set it up as well. More info at https://www.elastic.co/products/elasticsearch~~ Turns out it didn't work in production so I disabled it. Will be enabled in the future, no installation required as of now

4. Setup rails. Run the following as a regular user, **not as root** (for increased security)
`\curl -L https://get.rvm.io | bash -s stable --rails`
This should install RVM, ruby and rails assuming you want a single-user installation
(which is ok in most cases)

4. Set up rails, clone the repo and install the required gems:
```
source ~/.rvm/scripts/rvm
git clone https://github.com/kilves/hankiboard.git
cd hankiboard
bundle install
```

5. Set up the database configuration. Go to the `config` folder and create a file named `database.yml`. Fill it with the database configuration of your choice (mysql2, postgresql, sqlite3, etc). Refer to this documentation for help: https://gist.github.com/erichurst/961978

6. Create the databases you defined in the previous step.

7. Populate the databases:
`rake db:schema:load`

8. Start the server:
```
source ~/.rvm/scripts/rvm
rails s
```

More instructions coming (I just copied these from a readme template, ignore)
### Configuration

### Database creation

### Database initialization

### Deployment instructions

* ...
